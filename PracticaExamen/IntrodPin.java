package PracticaExamen;

import java.util.Scanner;

public class IntrodPin {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		int pin = 1234;
		int intentosGastados = 0;
		boolean salir = false;
		
		while(intentosGastados < 2 && salir == false) {
			intentosGastados ++;
			System.out.println("Introduzca su pin: ");
			int pinIntroducido = entrada.nextInt();
			if (pinIntroducido == pin) {
				System.out.println("Pin correcto");
				salir = true;
				
			}else {
				System.out.println("Pin incorrecto");
				if (intentosGastados == 2) {
					System.out.println("Intentos agotados");
				}
			}
		
		}
	System.out.println("Fin del programa");
	}

}
