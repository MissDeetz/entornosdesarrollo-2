package PracticaExamen;

public class Piramide {

	public static void main(String [] args) {
		
		int numFilas = 3;
		for(int i = numFilas; i > 0; i--) {
			for(int j= 0; j < i; j++) {
				System.out.print("p");
			}
			System.out.println();  
		}
		
		System.out.println("\n");
		
		int numFilas2 = 3;
		for(int i = numFilas2; i > 0; i--) {
			for(int j= 0; j <= numFilas2 - i; j++) {
				System.out.print("p");
			}
			System.out.println();  
		}
	}			
}

