package POO;

import ProyectoCuentas.CuentaBancaria;

public class TestCuentaBancaria {
		
		public static void main(String[] args) {
			CuentaBancaria cuenta1 = new CuentaBancaria ("Sandra", "ES203232", 0.05, 300);
			CuentaBancaria cuenta2 = new CuentaBancaria(cuenta1);
			System.out.println("El saldo de cuenta 1 es: " + cuenta1.getSaldo());
			System.out.println("El saldo de la cuenta 2 es: " + cuenta2.getSaldo());
			System.out.println("El nombre de la cuenta 2 es: " + cuenta2.getNombre());
			cuenta1.ingreso(100);
			cuenta1.reintegro(50);
			System.out.println("El saldo de la cuenta 1 es: " + cuenta1.getSaldo());	
		}
	}
//este es mi cambio en repositorio local 