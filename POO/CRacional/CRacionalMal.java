package POO.CRacional;

public class CRacionalMal {
	
	private int numerador; 
	private int denominador; 
	
	public CRacionalMal() {
		
	}
	//void es un m�todo que no devuelve nada
	public void asignarDatos(int num, int den)  {   
		 numerador = num;    
		 if (den == 0) den = 1; // el denominador no puede ser cero    
		 denominador = den;  
	 }    
	 
	public void visualizarRacional()  {    
		 System.out.println(numerador + "/" + denominador);  
	 }
	//metodo get devuelve el valor del atributo
	public int getNumerador() {
		return numerador;
	}
	//m�todo set asigna un valor 
	public void setNumerador(int numerador) {
		this.numerador = numerador;
	}
	public int getDenominador() {
		return denominador;
	}
	public void setDenominador(int denominador) {
		this.denominador = denominador;
	}    
	/**
	 * este m�todo suma el c�lculo del racional de 2 objetos.
	 * @param racional1
	 * @param racional2
	 * @return
	 */
	public int suma(CRacionalMal racional1, CRacionalMal racional2) {
		//puedo acceder al numerador sin 'get' porque estoy dentro de la misma clase.
		int resultado = racional1.numerador / racional1.denominador;
		int resultado2 = racional2.numerador / racional2.denominador;
		return resultado + resultado2;
	}
}
