package POO.CRacional;

public class TestCRacionalMal {
	 public static void main (String[] args)  {
		 
		    // Punto de entrada a la aplicación    
			 CRacionalMal r1 = new CRacionalMal(); 
			 // crear un objeto CRacional        
			 r1.asignarDatos(2, 5);    
			 r1.visualizarRacional(); 
			 CRacionalMal racional1 = new CRacionalMal();
			 racional1.setDenominador(5);
			 racional1.setNumerador(10);
			 CRacionalMal racional2 = new CRacionalMal();
			 racional2.setDenominador(2);
			 racional2.setNumerador(20);
			
			 int suma =  r1.suma(racional1, racional2);
			 System.out.println("la suma del racional es: " + suma);
	} 
}
