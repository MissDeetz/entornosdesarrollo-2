package estructurasdecontrol;
/*caracter: *
 * veces: 5
resultado= *****
 */

import java.io.IOException;
import java.util.Scanner;

public class Caracter {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Introduzca un caracter: ");
		char car = (char) System.in.read();
		for(int i = 0; i<5; i++)
			System.out.print(car);
	} 

}
