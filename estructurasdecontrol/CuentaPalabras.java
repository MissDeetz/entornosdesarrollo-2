package estructurasdecontrol;

import java.io.IOException;

public class CuentaPalabras {

	public static void main(String[] args)  throws IOException {
		
		/*Este programa realiza un pseudoc�digo que visualiza 
		 * en pantalla el n�mero de palabras que tiene 
		 * una frase acabada en punto.
		 */
		
		System.out.println("Introduzca una frase");
		int contadorPalabras = 0;
		char caracter = (char) System.in.read();
		boolean finPalabra = true;
		
		
		while(caracter != '.') {
			
			if (finPalabra == true && caracter != ' ')  {
				contadorPalabras++;
				finPalabra = false;
			} else if (caracter == ' ') {
				finPalabra = true;
			}
			caracter = (char)System.in.read();
			
		}
		System.out.println("Numero de palabras: " + contadorPalabras);
		
	}

}