package estructurasdecontrol;

import java.util.Scanner;

public class ConversorNotasSwitch {

	public static void main(String[] args) {


		
		/*Este programa convierte las calificaciones
		 * alfabéticas  I,F, B, N y S en calificaciones
		 * numéricas 4, 5, 6, 7 y 9 respectivamente.
		 * Utilizando la estructura switch
		 */
		
		Scanner entrada = new Scanner(System.in);
		
		//Introducimos por teclado las notas.
		
		 System.out.println("Introduce una nota alfabética");
		 char letra = entrada.next().charAt(0);   
		 
		 //Declaramos una nota numérica
		 int nota = 0;
		 
		 //Variable booleana para comprobar si la nota es correcta.
		 boolean notaCorrecta = true;
		 
		 //Convertimos la  nota alfabética a numérica.
		 switch (letra) {
		   case 'I':
		     nota = 4;
		     break;
		   case 'F':
		     nota = 5;
		     break;
		   case 'B':
		     nota = 6;
		     break;
		   case 'N':
		     nota= 7;
		     break;
		   case 'S':
		     nota = 9;
		     break;
		   default:
			 System.out.println("Esta nota no es válida");
			notaCorrecta = false;
			 break;
			 
				
		 }
		 
		 //Comprobamos si la nota es correcta
		 if(notaCorrecta) {
			 System.out.println("Tu nota es " +nota);
		 }
		 //Enviamos a pantalla la nota numérica
		 
	}
}
