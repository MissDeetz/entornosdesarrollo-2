package estructurasdecontrol;

import java.util.Scanner;

/*este programa visualiza en pantalla la potencia
 * de una base elevada a un exponente.
 */

public class Potencia {

	public static void main(String[] args) {
		
		//Introducimos la base y el exponente.
		Scanner entrada = new Scanner(System.in);		
		System.out.println("Introduzca una base: ");
		int base = entrada.nextInt();
		System.out.println("Introduzca un exponente: ");
		int exponente = entrada.nextInt();
		
		//calculamos la potencia
		//Inicializamos el acumulador
		int resultado = 1;
		for(int i = 1; i <= exponente; i++)
			resultado = resultado * base;
		//Enviar a pantalla el resultado
		System.out.println("El resultado es " + resultado);
		
		}
	}