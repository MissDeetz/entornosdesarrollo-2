package estructurasdecontrol;

import java.util.Scanner;

public class Password {

	public static void main(String[] args) {
	
		/*Este programa pide al usuario
		 * una contrase�a y muestra si es v�lida
		 * comparando con una fija.
		 */

		String contrase�a = "contrase�a";
				
		System.out.println("Introduzca su contrase�a");
		Scanner entrada = new Scanner(System.in);
		String contrase�aUsuario = entrada.next();
		
		if(contrase�a.equals(contrase�aUsuario.toLowerCase())) {
			System.out.println("La contrase�a es v�lida ");
		}
		else {
			System.out.println("La contrase�a no es v�lida ");
		}
			
	}

}
