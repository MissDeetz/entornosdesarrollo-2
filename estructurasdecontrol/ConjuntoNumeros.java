package estructurasdecontrol;

import java.util.Scanner;

public class ConjuntoNumeros {

	public static void main(String[] args) {
	
	/*Este programa muestra qu� n�meros son menores de 15,
	 * cu�ntos mayores de 55 y cu�ntos est�n comprendidos
	 * entre 45 y 55.
	 */
	
		int countmenor15 = 0;
		int countmayor55 = 0;
		int countentre45y55 = 0;
		
		for(int i = 1; i <= 6; i++) {
			
			//Pedimos al usuario que introduzca 6 n�meros
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduzca un n�mero " +i);
			int num = entrada.nextInt();
						
			if(num<15) {
				countmenor15 = countmenor15 +1;
				//countmenor15 ++;
			}
			else if(num>55) {
				countmayor55 = countmayor55 +1;
			}
			else if(num>=45 && num<=55) {
				countentre45y55 = countentre45y55 +1;
			}
		}	
		System.out.println("Los n�meros menores que 15 son " + countmenor15);
		System.out.println("Los n�meros mayores que 55 son " + countmayor55);
		System.out.println("Los n�meros entre 45 y 55 son " + countentre45y55);
	
	}

}	