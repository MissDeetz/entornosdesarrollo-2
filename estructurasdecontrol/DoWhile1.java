package estructurasdecontrol;

import java.io.IOException;

public class DoWhile1 {

	public static void main(String[] args) throws IOException {
		
		/*Introduce por teclado una respuesta v�lida.
		 * La respuesta es v�lida cuando sea una 's' may�scula
		 * o una 'n' min�scula
		 */
		

		char respuesta;
		System.out.println("Introduzca la respuesta");
		do {
			respuesta = (char)System.in.read();
			respuesta = Character.toLowerCase(respuesta);
			//Limpiamos el buffer de teclado
			System.in.skip(2);
			if(respuesta != 's' && respuesta != 'n')
				System.out.println("error: teclea una 's' o una 'n'");
		}while (respuesta != 's' && respuesta != 'n');
		
		System.out.println("fin del programa");
		
		/*cuando metemos un caracter por teclado, este se guarda con 
		 \n*/

	}

}
