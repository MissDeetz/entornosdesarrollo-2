package estructurasdecontrol;

import java.io.IOException;

public class ControlD {

	public static void main(String[] args) throws IOException {
		
	final char EOF = (char)-1;
	System.out.println(EOF);
	System.out.println("introduce frase (ctrlD para terminar):");
	
	char car = (char) System.in.read();
	
	while(car!= EOF)
		
		System.out.print(car);
		car=(char)System.in.read();
		System.out.print(car);
	}//fin mientras

}//fin main
