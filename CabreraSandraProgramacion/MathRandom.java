package CabreraSandraProgramacion;
import java.lang.Math;
import java.util.Scanner;
/*Realizar un juego que consiste en acertar un num desconocido
 * generado aleatoriamente entre 1 y 100.
 * El proceso termina cuando acierta o se rinde introduciendo -1
 */
public class MathRandom {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca num1: Pulsa -1 para salir");
		int num;
		int numSecreto= (int)(Math.random()* 100+1);

	do {
		num= entrada.nextInt();//si haces esto dentro del bucle sigue pidiendo num sin tener que repetir el texto.
		if(numSecreto < num) {
			System.out.println("es menor");
		}else {
			System.out.println("es mayor");
			
			}
	}
		while(numSecreto!= num && num!= -1);
			
			System.out.println("num es:" + numSecreto);
		
		if(numSecreto ==num){
			System.out.println("has acertado");
		}else {
				System.out.println("te has rendido");
		}		
	}	
}